#pragma once

#include "wParameters.h"

class cTimerHighRes
{
	public:
		cTimerHighRes() : m_beg(clock_::now()) {}

		//Timer reset
		void reset()
		{
			m_beg = clock_::now();
		}

		//Returns time elapsed in milliseconds since last reset or object creation
		double elapsed() const
		{
			return chrono::duration_cast<chrono::milliseconds>(clock_::now() - m_beg).count();
		}

	private:
		typedef chrono::high_resolution_clock clock_;
		chrono::time_point<clock_> m_beg;
};
