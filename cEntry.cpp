#include "cEntry.h"

wxIMPLEMENT_APP(cEntry);

cEntry::cEntry()
{
	//Initialize variables and pointers
    pMain = NULL;
    masterRS485 = NULL;
    wxChecker = NULL;
}

bool cEntry::OnInit()
{
    wxApp::SetAppName(wxString(_T("ARCz Test")));

    {
        wxLogNull logNo; //suppress information message
        wxChecker = new wxSingleInstanceChecker(_T(".arcztest_temp"));
    }

    //Check if process already exists
    if (wxChecker->IsAnotherRunning())
    {
		if (wxMessageBox(_T("\nProces już istnieje.\nZakończyć poprzedni proces?"),
				_T("ARCz RS485 Test"), wxOK | wxCANCEL | wxICON_INFORMATION) == wxOK)
		{
			//Get PID of previous instance
	        string command = "kill $(pidof -o " + to_string(::getpid()) + " ARCzTest)";
	        int32_t rtn = system(command.c_str());

	        //Delete old single instance checker
	        command = "rm " + wxString::FromUTF8(getenv("HOME")).ToStdString() + "/.arcztest_temp";
	        rtn = system(command.c_str());
	        rtn = rtn + 1; //to prevent compiler warnings

	        //Create new single instance checker
	        wxLogNull logNo; //suppress information message
			delete wxChecker;
	        wxChecker = new wxSingleInstanceChecker(_T(".arcztest_temp"));
		}
		else
		{
			delete wxChecker;
			wxChecker = NULL;
			return false;
		}
    }

	//Start Modbus master
	masterRS485 = new cMasterRS485(this);

    //Create Main window
    pMain = new cMain(this, masterRS485);

	//Show main window after delay
	pMain->Show();

	return true;
}

int cEntry::OnExit()
{
    //Close Modbus gateway and free resources
	masterRS485->CloseRS485();

    //Free pointers
    delete masterRS485;
    delete wxChecker;
    masterRS485 = NULL;
    wxChecker = NULL;

    return 0;
}
