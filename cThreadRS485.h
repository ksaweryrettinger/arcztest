#pragma once

#include "cMasterRS485.h"
#include "wParameters.h"

class cMain;
class cMasterRS485;
class cThreadRS485;

/* Looping thread for RS485 communication */
class cThreadRS485 : public wxThread
{
	public:
		cThreadRS485(cMasterRS485*);
		~cThreadRS485();

	protected:
		virtual ExitCode Entry();

	private:
		cMasterRS485* master;
		int32_t serial_port;
		string errno_str;
		uint8_t read_buf[2] = { 0 };
		uint8_t write_buf[7] = { 0 };
		uint8_t num_bytes;

	private: //data buffers
};
