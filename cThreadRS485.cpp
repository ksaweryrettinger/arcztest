#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#include "cThreadRS485.h"

cThreadRS485::cThreadRS485(cMasterRS485* master) : wxThread(wxTHREAD_DETACHED)
{
	//Constructor variables
	this->master = master;
	num_bytes = 0;
	serial_port = open("/dev/ttyACM0", O_RDWR);

	//Create new termios struct to configure serial port
	struct termios tty;

	// Read in existing settings, and handle any error
	if (tcgetattr(serial_port, &tty) != 0) errno_str = strerror(errno);

	//UART Configuration
	tty.c_cflag |= PARENB; //Set parity bit, enabling parity
	tty.c_cflag &= ~CSTOPB; //Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag &= ~CSIZE; //Clear all bits that set the data size
	tty.c_cflag |= CS8; //8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; //Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; //Turn on READ & ignore ctrl lines (CLOCAL = 1)
	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; //Disable echo
	tty.c_lflag &= ~ECHOE; //Disable erasure
	tty.c_lflag &= ~ECHONL; //Disable new-line echo
	tty.c_lflag &= ~ISIG; //Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes
	tty.c_oflag &= ~OPOST; //Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; //Prevent conversion of newline to carriage return/line feed
	tty.c_cc[VTIME] = 1;  //maximum wait time (100ms)
	tty.c_cc[VMIN] = 2; //minimum number of characters

	//Set in/out baud rate to be 19200
	cfsetispeed(&tty, B19200);
	cfsetospeed(&tty, B19200);

	// Save serial port settings, also checking for errors
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) errno_str = strerror(errno);
}

wxThread::ExitCode cThreadRS485::Entry()
{
	while (!TestDestroy())
	{
		//Clear read buffer and byte count
		read_buf[0] = 0;
		read_buf[1] = 0;
		num_bytes = 0;

		//Read serial port
		num_bytes = read(serial_port, &read_buf, sizeof(read_buf));

		//Check if new message received
		if (read_buf[0] == 'A') //TUNER A
		{
			if (read_buf[1] == '?') //data request received
			{
				//Send tuner data
				//wxCriticalSectionLocker mb_lock(master->data_guard);
				ssize_t temp = write(serial_port, master->DataTunerA, sizeof(master->DataTunerA));
			}
			else //button configuration received
			{
				//Send response
				write_buf[0] = 'a';
				write_buf[1] = read_buf[1];
				ssize_t temp = write(serial_port, write_buf, 2);

				//Store new button configuration
				//wxCriticalSectionLocker mb_lock(master->data_guard);
				master->ButtonsTunerA = read_buf[1];
			}
		}
		else if (read_buf[0] == 'B') //TUNER B
		{
			if (read_buf[1] == '?') //data request received
			{
				//Send tuner data
				//wxCriticalSectionLocker mb_lock(master->data_guard);
				ssize_t temp = write(serial_port, master->DataTunerB, sizeof(master->DataTunerB));
			}
			else //button configuration received
			{
				//Send response
				write_buf[0] = 'b';
				write_buf[1] = read_buf[1];
				ssize_t temp = write(serial_port, write_buf, 2);

				//Store new button configuration
				//wxCriticalSectionLocker mb_lock(master->data_guard);
				master->ButtonsTunerB = read_buf[1];
			}
		}
	}

	return (wxThread::ExitCode) 0;
}

cThreadRS485::~cThreadRS485()
{
	close(serial_port);
	master->ThreadRS485 = NULL;
}
