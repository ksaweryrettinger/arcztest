#pragma once

#include "wParameters.h"
#include "cEntry.h"
#include "cMain.h"
#include "cThreadRS485.h"

class cEntry;
class cMain;
class cThreadRS485;

/* Modbus Gateway class */
class cMasterRS485
{
	public:
		cMasterRS485(cEntry*);
		void CloseRS485(void);

	public: //class pointers and flags
		cMain* pMain;

	public: //critical sections
		wxCriticalSection data_guard;

	public: //threads
		cThreadRS485* ThreadRS485;

	public: //SHARED DATA
		uint8_t ButtonsTunerA;
		uint8_t ButtonsTunerB;
		uint8_t DataTunerA[7] = { 0 };
		uint8_t DataTunerB[7] = { 0 };
};
