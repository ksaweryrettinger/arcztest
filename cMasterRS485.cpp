#include "cMasterRS485.h"

cMasterRS485::cMasterRS485(cEntry* pEntry)
{
    this->pMain = pMain;

    //Tuner A
    ButtonsTunerA = 0;
    DataTunerA[0] = 'a';
    DataTunerA[1] = '0';
    DataTunerA[2] = '0';
    DataTunerA[3] = '0';
    DataTunerA[4] = '0';

    //Tuner B
    ButtonsTunerB = 0;
    DataTunerB[0] = 'b';
    DataTunerB[1] = '0';
    DataTunerB[2] = '0';
    DataTunerB[3] = '0';
    DataTunerB[4] = '0';

    //Start RS485 thread
    ThreadRS485 = new cThreadRS485(this);
    ThreadRS485->Run();
}

//Close RS485 connection
void cMasterRS485::CloseRS485(void)
{
	//Terminate RTU read thread
	if (ThreadRS485 != NULL) ThreadRS485->Delete();
    while (ThreadRS485 != NULL) wxMilliSleep(1);
}
