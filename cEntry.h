#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "wParameters.h"
#include "cMain.h"
#include "cMasterRS485.h"

class cMain;
class cMasterRS485;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cMasterRS485* masterRS485;
    wxSingleInstanceChecker* wxChecker;

    friend class cMasterRS485;
};

#pragma GCC diagnostic pop
