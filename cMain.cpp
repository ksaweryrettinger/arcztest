#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
    EVT_CLOSE(cMain::OnClose)
	EVT_TIMER(20001, cMain::OnRefreshDisplay)
wxEND_EVENT_TABLE()

BEGIN_EVENT_TABLE(cMyPanel, wxPanel)
    EVT_KEY_DOWN(cMyPanel::OnKeyDown)
END_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry, cMasterRS485* masterRS485) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(500, 500), wxSize(1000, 225), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("ARCz RS485 Test"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(225, 225, 225));

    //CONSTRUCTOR VARIABLES
    this->pEntry = pEntry;
    this->masterRS485 = masterRS485;

    //OTHER VARIABLES
    ButtonsTunerA = 0;
    ButtonsTunerB = 0;

	//PANEL
	panMain = new cMyPanel(this, masterRS485);

	//MAIN IMAGES
	imgFutaba = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/ARCzTest/Images/FutabaGrid.png"), wxBITMAP_TYPE_PNG), wxPoint(10, 10), wxSize(980, 180));

	for (uint8_t i = 0; i < NUM_CHAR_FUTABA; i++)
	{
		if (i < 10) txtFutaba[i] = new wxStaticText(this, wxID_ANY, _T("-"), wxPoint(11 + i*49, 42), wxSize(49, 30), wxALIGN_CENTRE_HORIZONTAL);
		else if (i < 20) txtFutaba[i] = new wxStaticText(this, wxID_ANY, _T("-"), wxPoint(10 + i*49, 42), wxSize(49, 30), wxALIGN_CENTRE_HORIZONTAL);
		else if (i < 30) txtFutaba[i] = new wxStaticText(this, wxID_ANY, _T("-"), wxPoint(11 + (i - 20)*49, 132), wxSize(49, 30), wxALIGN_CENTRE_HORIZONTAL);
		else if (i < 40) txtFutaba[i] = new wxStaticText(this, wxID_ANY, _T("-"), wxPoint(10 + (i - 20)*49, 132), wxSize(49, 30), wxALIGN_CENTRE_HORIZONTAL);
		txtFutaba[i]->SetFont(wxFont(20, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	}

	//DISPLAY REFRESH TIMER
    tRefreshTimer = new wxTimer(this, 20001);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);
}

//Panel constructor
cMyPanel::cMyPanel(cMain* pMain, cMasterRS485* masterRS485) : wxPanel(pMain)
{
	//RS485 master
	this->masterRS485 = masterRS485;

    //OTHER VARIABLES
    ButtonsTunerA = 0;
    ButtonsTunerB = 0;
}

//Refresh main frame
void cMain::RefreshDisplay(void)
{
	/******************************************* Copy RS485 Data ******************************************/

	if (masterRS485 != NULL)
	{
		wxCriticalSectionLocker local_lock(masterRS485->data_guard);

		ButtonsTunerA = masterRS485->ButtonsTunerA;
		ButtonsTunerB = masterRS485->ButtonsTunerB;

		for (uint8_t i = 0; i < NUM_BYTES_TUNER; i++)
		{
			DataTunerA[i] = masterRS485->DataTunerA[i];
			DataTunerB[i] = masterRS485->DataTunerB[i];
		}
	}

	/*************************************** Update Display (First Row) *************************************/

	//Precyzyjny/Zgrubny
	if (!(ButtonsTunerA & BUTTON_AR)) txtFutaba[0]->SetLabel(_T(" ")); //empty space
	else if (!(ButtonsTunerA & BUTTON_PZ)) txtFutaba[0]->SetLabel(_T('P')); //precyzyjny
	else if (ButtonsTunerA & BUTTON_PZ) txtFutaba[0]->SetLabel(_T('Z')); //zgrubny

	//Automatyczna/Ręczna
	if (!(ButtonsTunerA & BUTTON_AR)) txtFutaba[1]->SetLabel(_T('A')); //automatyczna
	else if (ButtonsTunerA & BUTTON_AR) txtFutaba[1]->SetLabel(_T('R')); //ręczna

	//R/F Errors
	if (DataTunerA[6] & 0x10) txtFutaba[2]->SetLabel(_T('R'));
	else txtFutaba[2]->SetLabel(_T(" "));

	//Left Movement Symbols
	if (DataTunerA[5] & 0x01) txtFutaba[3]->SetLabel(_T('<')); //moving left
	else if (DataTunerA[5] & 0x02) txtFutaba[3]->SetLabel(_T("<<")); //moving fast left
	else txtFutaba[3]->SetLabel(_T(" ")); //not moving

	//Data OR Left/Right Extremes
	if (!(DataTunerA[6] & 0x01) && !(DataTunerA[6] & 0x02)) //display Reading
	{
		txtFutaba[4]->SetLabel(_T('1'));
		txtFutaba[5]->SetLabel(wxString::Format(wxT("%c"), DataTunerA[1]));
		txtFutaba[6]->SetLabel(_T('.'));
		txtFutaba[7]->SetLabel(wxString::Format(wxT("%c"), DataTunerA[2]));
	}
	else if (DataTunerA[6] & 0x01) //left extreme
	{
		txtFutaba[4]->SetLabel(_T('K'));
		txtFutaba[5]->SetLabel(_T('R'));
		txtFutaba[6]->SetLabel(_T('N'));
		txtFutaba[7]->SetLabel(_T('-'));
	}
	else if (DataTunerA[6] & 0x02) //right extreme
	{
		txtFutaba[4]->SetLabel(_T('K'));
		txtFutaba[5]->SetLabel(_T('R'));
		txtFutaba[6]->SetLabel(_T('N'));
		txtFutaba[7]->SetLabel(_T('+'));
	}

	//Right Movement Symbols
	if (DataTunerA[5] & 0x04) txtFutaba[8]->SetLabel(_T('>')); //moving right
	else if (DataTunerA[5] & 0x08) txtFutaba[8]->SetLabel(_T(">>")); //moving fast right
	else txtFutaba[8]->SetLabel(_T(" ")); //not moving

	//Empty Spaces
	txtFutaba[9]->SetLabel(_T(" "));
	txtFutaba[10]->SetLabel(_T(" "));

	//Left Movement Symbols
	if (DataTunerB[5] & 0x01) txtFutaba[11]->SetLabel(_T('<')); //moving left
	else if (DataTunerB[5] & 0x02) txtFutaba[11]->SetLabel(_T("<<")); //moving fast left
	else txtFutaba[11]->SetLabel(_T(" ")); //not moving

	//Data OR Left/Right Extremes
	if (!(DataTunerB[6] & 0x01) && !(DataTunerB[6] & 0x02)) //display Reading
	{
		txtFutaba[12]->SetLabel(_T('1'));
		txtFutaba[13]->SetLabel(wxString::Format(wxT("%c"), DataTunerB[1]));
		txtFutaba[14]->SetLabel(_T('.'));
		txtFutaba[15]->SetLabel(wxString::Format(wxT("%c"), DataTunerB[2]));
	}
	else if (DataTunerB[6] & 0x01) //left extreme
	{
		txtFutaba[12]->SetLabel(_T('K'));
		txtFutaba[13]->SetLabel(_T('R'));
		txtFutaba[14]->SetLabel(_T('N'));
		txtFutaba[15]->SetLabel(_T('-'));
	}
	else if (DataTunerB[6] & 0x02) //right extreme
	{
		txtFutaba[12]->SetLabel(_T('K'));
		txtFutaba[13]->SetLabel(_T('R'));
		txtFutaba[14]->SetLabel(_T('N'));
		txtFutaba[15]->SetLabel(_T('+'));
	}

	//Right Movement Symbols
	if (DataTunerB[5] & 0x04) txtFutaba[16]->SetLabel(_T('>')); //moving right
	else if (DataTunerB[5] & 0x08) txtFutaba[16]->SetLabel(_T(">>")); //moving fast right
	else txtFutaba[16]->SetLabel(_T(" ")); //not moving

	//R/F Errors
	if (DataTunerB[6] & 0x10) txtFutaba[17]->SetLabel(_T('R'));
	else txtFutaba[17]->SetLabel(_T(" "));

	//Automatyczna/Ręczna
	if (!(ButtonsTunerB & BUTTON_AR)) txtFutaba[18]->SetLabel(_T('A')); //automatyczna
	else if (ButtonsTunerB & BUTTON_AR) txtFutaba[18]->SetLabel(_T('R')); //ręczna

	//Precyzyjny/Zgrubny
	if (!(ButtonsTunerB & BUTTON_AR)) txtFutaba[19]->SetLabel(_T(" ")); //empty space
	else if (!(ButtonsTunerB & BUTTON_PZ)) txtFutaba[19]->SetLabel(_T('P')); //precyzyjny
	else if (ButtonsTunerB & BUTTON_PZ) txtFutaba[19]->SetLabel(_T('Z')); //zgrubny

	/************************************** Update Display (Second Row) *************************************/

	//Plus (+)/Zero (0)/Minus (-)
	if (!(ButtonsTunerA & (BUTTON_PM_1 | BUTTON_PM_2))) txtFutaba[20]->SetLabel(_T(" ")); //Zero (0)
	else if (ButtonsTunerA & BUTTON_PM_1) txtFutaba[20]->SetLabel(_T('-')); //Minus (-)
	else if (ButtonsTunerA & BUTTON_PM_2) txtFutaba[20]->SetLabel(_T('+')); //Plus (+)

	//Zerowanie/Strojenie
	if (!(ButtonsTunerA & BUTTON_ZS)) txtFutaba[21]->SetLabel(_T('Z')); //Zerowanie
	else if (ButtonsTunerA & BUTTON_ZS) txtFutaba[21]->SetLabel(_T('S')); //Strojenie

	//R/F Errors
	if (DataTunerA[6] & 0x10) txtFutaba[22]->SetLabel(_T('F'));
	else txtFutaba[22]->SetLabel(_T(" "));

	//Left Movement Symbols
	if (DataTunerA[5] & 0x10) txtFutaba[23]->SetLabel(_T('<')); //moving left
	else if (DataTunerA[5] & 0x20) txtFutaba[23]->SetLabel(_T("<<")); //moving fast left
	else txtFutaba[23]->SetLabel(_T(" ")); //not moving

	//Data OR Left/Right Extremes
	if (!(DataTunerA[6] & 0x04) && !(DataTunerA[6] & 0x08)) //display Reading
	{
		txtFutaba[24]->SetLabel(_T('±'));
		txtFutaba[25]->SetLabel(wxString::Format(wxT("%c"), DataTunerA[3]));
		txtFutaba[26]->SetLabel(wxString::Format(wxT("%c"), DataTunerA[4]));
		txtFutaba[27]->SetLabel(_T('%'));
	}
	else if (DataTunerA[6] & 0x04) //left extreme
	{
		txtFutaba[24]->SetLabel(_T('K'));
		txtFutaba[25]->SetLabel(_T('R'));
		txtFutaba[26]->SetLabel(_T('N'));
		txtFutaba[27]->SetLabel(_T('-'));
	}
	else if (DataTunerA[6] & 0x08) //right extreme
	{
		txtFutaba[24]->SetLabel(_T('K'));
		txtFutaba[25]->SetLabel(_T('R'));
		txtFutaba[26]->SetLabel(_T('N'));
		txtFutaba[27]->SetLabel(_T('+'));
	}

	//Right Movement Symbols
	if (DataTunerA[5] & 0x40) txtFutaba[28]->SetLabel(_T('>')); //moving right
	else if (DataTunerA[5] & 0x80) txtFutaba[28]->SetLabel(_T(">>")); //moving fast right
	else txtFutaba[28]->SetLabel(_T(" ")); //not moving

	//Empty Space
	txtFutaba[29]->SetLabel(_T(" "));
	txtFutaba[30]->SetLabel(_T(" "));

	//Left Movement Symbols
	if (DataTunerB[5] & 0x10) txtFutaba[31]->SetLabel(_T('<')); //moving left
	else if (DataTunerB[5] & 0x20) txtFutaba[31]->SetLabel(_T("<<")); //moving fast left
	else txtFutaba[31]->SetLabel(_T(" ")); //not moving

	//Data OR Left/Right Extremes
	if (!(DataTunerB[6] & 0x04) && !(DataTunerB[6] & 0x08)) //display Reading
	{
		txtFutaba[32]->SetLabel(_T('±'));
		txtFutaba[33]->SetLabel(wxString::Format(wxT("%c"), DataTunerB[3]));
		txtFutaba[34]->SetLabel(wxString::Format(wxT("%c"), DataTunerB[4]));
		txtFutaba[35]->SetLabel(_T('%'));
	}
	else if (DataTunerB[6] & 0x04) //left extreme
	{
		txtFutaba[32]->SetLabel(_T('K'));
		txtFutaba[33]->SetLabel(_T('R'));
		txtFutaba[34]->SetLabel(_T('N'));
		txtFutaba[35]->SetLabel(_T('-'));
	}
	else if (DataTunerB[6] & 0x08) //right extreme
	{
		txtFutaba[32]->SetLabel(_T('K'));
		txtFutaba[33]->SetLabel(_T('R'));
		txtFutaba[34]->SetLabel(_T('N'));
		txtFutaba[35]->SetLabel(_T('+'));
	}

	//Right Movement Symbols
	if (DataTunerB[5] & 0x40) txtFutaba[36]->SetLabel(_T('>')); //moving right
	else if (DataTunerB[5] & 0x80) txtFutaba[36]->SetLabel(_T(">>")); //moving fast right
	else txtFutaba[36]->SetLabel(_T(" ")); //not moving

	//R/F Errors
	if (DataTunerB[6] & 0x10) txtFutaba[37]->SetLabel(_T('F'));
	else txtFutaba[37]->SetLabel(_T(" "));

	//Zerowanie/Strojenie
	if (!(ButtonsTunerB & BUTTON_ZS)) txtFutaba[38]->SetLabel(_T('Z')); //Zerowanie
	else if (ButtonsTunerB & BUTTON_ZS) txtFutaba[38]->SetLabel(_T('S')); //Strojenie

	//Plus (+)/Zero (0)/Minus (-)
	if (!(ButtonsTunerB & (BUTTON_PM_1 | BUTTON_PM_2))) txtFutaba[39]->SetLabel(_T(" ")); //Zero (0)
	else if (ButtonsTunerB & BUTTON_PM_1) txtFutaba[39]->SetLabel(_T('-')); //Minus (-)
	else if (ButtonsTunerB & BUTTON_PM_2) txtFutaba[39]->SetLabel(_T('+')); //Plus (+)

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMain::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

void cMyPanel::GetMasterData(void)
{
	{
		wxCriticalSectionLocker local_lock(masterRS485->data_guard);

		ButtonsTunerA = masterRS485->ButtonsTunerA;
		ButtonsTunerB = masterRS485->ButtonsTunerB;

		for (uint8_t i = 0; i < NUM_BYTES_TUNER; i++)
		{
			DataTunerA[i] = masterRS485->DataTunerA[i];
			DataTunerB[i] = masterRS485->DataTunerB[i];
		}
	}
}

void cMyPanel::OnKeyDown(wxKeyEvent& evt)
{
	wxChar uc = evt.GetUnicodeKey();

	if (uc != WXK_NONE) //normal keys
	{
		switch (uc)
		{
			case 'P': //signal right extremes

				GetMasterData();

				//Tuner A
				if (!(DataTunerA[6] & 0x02)) //switch ON signal
				{
					DataTunerA[6] |= 0x02;
					DataTunerA[6] |= 0x08;
					DataTunerA[6] &= ~(0x01); //switch OFF left signal
					DataTunerA[6] &= ~(0x04); //switch OFF left signal
				}
				else if (DataTunerA[6] & 0x02) //switch OFF signal
				{
					DataTunerA[6] &= ~(0x02);
					DataTunerA[6] &= ~(0x08);
				}

				//Tuner B
				if (!(DataTunerB[6] & 0x02)) //switch ON signal
				{
					DataTunerB[6] |= 0x02;
					DataTunerB[6] |= 0x08;
					DataTunerB[6] &= ~(0x01); //switch OFF left signal
					DataTunerB[6] &= ~(0x04); //switch OFF left signal
				}
				else if (DataTunerB[6] & 0x02) //switch OFF signal
				{
					DataTunerB[6] &= ~(0x02);
					DataTunerB[6] &= ~(0x08);
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);
					masterRS485->DataTunerA[6] = DataTunerA[6];
					masterRS485->DataTunerB[6] = DataTunerB[6];
				}

				break;

			case 'L': //signal left extremes

				GetMasterData();

				//Tuner A
				if (!(DataTunerA[6] & 0x01)) //switch ON signal
				{
					DataTunerA[6] |= 0x01;
					DataTunerA[6] |= 0x04;
					DataTunerA[6] &= ~(0x02); //switch OFF right signal
					DataTunerA[6] &= ~(0x08); //switch OFF right signal
				}
				else if (DataTunerA[6] & 0x01) //switch OFF signal
				{
					DataTunerA[6] &= ~(0x01);
					DataTunerA[6] &= ~(0x04);
				}

				//Tuner B
				if (!(DataTunerB[6] & 0x01)) //switch ON signal
				{
					DataTunerB[6] |= 0x01;
					DataTunerB[6] |= 0x04;
					DataTunerB[6] &= ~(0x02); //switch OFF right signal
					DataTunerB[6] &= ~(0x08); //switch OFF right signal
				}
				else if (DataTunerB[6] & 0x01) //switch OFF signal
				{
					DataTunerB[6] &= ~(0x01);
					DataTunerB[6] &= ~(0x04);
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);
					masterRS485->DataTunerA[6] = DataTunerA[6];
					masterRS485->DataTunerB[6] = DataTunerB[6];
				}

				break;

			case 'R': //signal R/F errors

				GetMasterData();

				//Tuner A
				if (!(DataTunerA[6] & 0x10)) DataTunerA[6] |= 0x10; //switch ON signal
				else DataTunerA[6] &= ~(0x10); //switch OFF signal

				//Tuner A
				if (!(DataTunerB[6] & 0x10)) DataTunerB[6] |= 0x10; //switch ON signal
				else DataTunerB[6] &= ~(0x10); //switch OFF signal

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);
					masterRS485->DataTunerA[6] = DataTunerA[6];
					masterRS485->DataTunerB[6] = DataTunerB[6];
				}

				break;
		}
	}
	else //special keys
	{
		switch (evt.GetKeyCode())
		{
			case WXK_LEFT:  //move tuners left

				GetMasterData();

				//Tuner A
				if (DataTunerA[5] & 0x08) //already moving fast right
				{
					DataTunerA[5] &= ~(0x08);
					DataTunerA[5] &= ~(0x80);
					DataTunerA[5] |= 0x04;
					DataTunerA[5] |= 0x40;
				}
				else if (DataTunerA[5] & 0x04) //already moving right
				{
					DataTunerA[5] &= ~(0x04);
					DataTunerA[5] &= ~(0x40);
				}
				else if (!(DataTunerA[5] & 0x01) && !(DataTunerA[5] & 0x02)) //start moving left
				{
					DataTunerA[5] |= 0x01;
					DataTunerA[5] |= 0x10;
				}
				else if (!(DataTunerA[5] & 0x02)) //start moving fast left
				{
					DataTunerA[5] &= ~(0x01);
					DataTunerA[5] &= ~(0x10);
					DataTunerA[5] |= 0x02;
					DataTunerA[5] |= 0x20;
				}

				//Tuner B
				if (DataTunerB[5] & 0x08) //already moving fast right
				{
					DataTunerB[5] &= ~(0x08);
					DataTunerB[5] &= ~(0x80);
					DataTunerB[5] |= 0x04;
					DataTunerB[5] |= 0x40;
				}
				else if (DataTunerB[5] & 0x04) //already moving right
				{
					DataTunerB[5] &= ~(0x04);
					DataTunerB[5] &= ~(0x40);
				}
				else if (!(DataTunerB[5] & 0x01) && !(DataTunerB[5] & 0x02)) //start moving left
				{
					DataTunerB[5] |= 0x01;
					DataTunerB[5] |= 0x10;
				}
				else if (!(DataTunerB[5] & 0x02)) //start moving fast left
				{
					DataTunerB[5] &= ~(0x01);
					DataTunerB[5] &= ~(0x10);
					DataTunerB[5] |= 0x02;
					DataTunerB[5] |= 0x20;
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);
					masterRS485->DataTunerA[5] = DataTunerA[5];
					masterRS485->DataTunerB[5] = DataTunerB[5];
				}

				break;

			case WXK_RIGHT: //move tuners right

				GetMasterData();

				//Tuner A
				if (DataTunerA[5] & 0x02) //already moving fast left
				{
					DataTunerA[5] &= ~(0x02);
					DataTunerA[5] &= ~(0x20);
					DataTunerA[5] |= 0x01;
					DataTunerA[5] |= 0x10;
				}
				else if (DataTunerA[5] & 0x01) //already moving left
				{
					DataTunerA[5] &= ~(0x01);
					DataTunerA[5] &= ~(0x10);
				}
				else if (!(DataTunerA[5] & 0x04) && !(DataTunerA[5] & 0x08)) //start moving right
				{
					DataTunerA[5] |= 0x04;
					DataTunerA[5] |= 0x40;
				}
				else if (!(DataTunerA[5] & 0x08)) //start moving fast right
				{
					DataTunerA[5] &= ~(0x04);
					DataTunerA[5] &= ~(0x40);
					DataTunerA[5] |= 0x08;
					DataTunerA[5] |= 0x80;
				}

				//Tuner B
				if (DataTunerB[5] & 0x02) //already moving fast left
				{
					DataTunerB[5] &= ~(0x02);
					DataTunerB[5] &= ~(0x20);
					DataTunerB[5] |= 0x01;
					DataTunerB[5] |= 0x10;
				}
				else if (DataTunerB[5] & 0x01) //already moving left
				{
					DataTunerB[5] &= ~(0x01);
					DataTunerB[5] &= ~(0x10);
				}
				else if (!(DataTunerB[5] & 0x04) && !(DataTunerB[5] & 0x08)) //start moving right
				{
					DataTunerB[5] |= 0x04;
					DataTunerB[5] |= 0x40;
				}
				else if (!(DataTunerB[5] & 0x08)) //start moving fast right
				{
					DataTunerB[5] &= ~(0x04);
					DataTunerB[5] &= ~(0x40);
					DataTunerB[5] |= 0x08;
					DataTunerB[5] |= 0x80;
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);
					masterRS485->DataTunerA[5] = DataTunerA[5];
					masterRS485->DataTunerB[5] = DataTunerB[5];
				}

				break;

			case WXK_UP: //increment tuner settings

				GetMasterData();

				//Tuner A Large
				if (DataTunerA[2] < '9')
				{
					DataTunerA[2]++;
				}
				else if (DataTunerA[1] < '9')
				{
					DataTunerA[1]++;
					DataTunerA[2] = '0';
				}

				//Tuner A Small
				if (DataTunerA[4] < '9')
				{
					DataTunerA[4]++;
				}
				else if (DataTunerA[3] < '9')
				{
					DataTunerA[3]++;
					DataTunerA[4] = '0';
				}

				//Tuner B Large
				if (DataTunerB[2] < '9')
				{
					DataTunerB[2]++;
				}
				else if (DataTunerB[1] < '9')
				{
					DataTunerB[1]++;
					DataTunerB[2] = '0';
				}

				//Tuner B Small
				if (DataTunerB[4] < '9')
				{
					DataTunerB[4]++;
				}
				else if (DataTunerB[3] < '9')
				{
					DataTunerB[3]++;
					DataTunerB[4] = '0';
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);

					for (uint8_t i = 1; i < 5; i++)
					{
						masterRS485->DataTunerA[i] = DataTunerA[i];
						masterRS485->DataTunerB[i] = DataTunerB[i];
					}
				}

				break;

			case WXK_DOWN: //decrement tuner settings

				GetMasterData();

				//Tuner A Large
				if (DataTunerA[2] > '0')
				{
					DataTunerA[2]--;
				}
				else if (DataTunerA[1] > '0')
				{
					DataTunerA[1]--;
					DataTunerA[2] = '9';
				}

				//Tuner A Small
				if (DataTunerA[4] > '0')
				{
					DataTunerA[4]--;
				}
				else if (DataTunerA[3] > '0')
				{
					DataTunerA[3]--;
					DataTunerA[4] = '9';
				}

				//Tuner B Large
				if (DataTunerB[2] > '0')
				{
					DataTunerB[2]--;
				}
				else if (DataTunerB[1] > '0')
				{
					DataTunerB[1]--;
					DataTunerB[2] = '9';
				}

				//Tuner B Small
				if (DataTunerB[4] > '0')
				{
					DataTunerB[4]--;
				}
				else if (DataTunerB[3] > '0')
				{
					DataTunerB[3]--;
					DataTunerB[4] = '9';
				}

				//Copy data to RS485 masterRS485
				{
					wxCriticalSectionLocker local_lock(masterRS485->data_guard);

					for (uint8_t i = 1; i < 5; i++)
					{
						masterRS485->DataTunerA[i] = DataTunerA[i];
						masterRS485->DataTunerB[i] = DataTunerB[i];
					}
				}

				break;
		}
	}
}

/* Frame closed */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp();
}

void cMain::TerminateApp(void)
{
	if (tRefreshTimer->IsRunning()) tRefreshTimer->Stop();
	if (masterRS485 != NULL) masterRS485->pMain = NULL;
	Destroy();
}
