#pragma once

//EXTERNAL LIBRARIES
#include <stdio.h>
#include <string.h>
#include <locale>
#include <unistd.h>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <algorithm>
#include <chrono>
#include <wx/wx.h>
#include <wx/snglinst.h>
#include <wx/progdlg.h>

//NAMESPACES
using namespace std;

//THREAD DELAYS
const int32_t DELAY_REFRESH_DISPLAY = 50;  //ms, main display refresh

const int32_t BUTTON_AR   = 0x01;  //automatyczna (0) / ręczna (1)
const int32_t BUTTON_ZS   = 0x02;  //zerowanie (0) / strojenie (1)
const int32_t BUTTON_PZ   = 0x04;  //precyzyjny (0) / zgrubny (1)
const int32_t BUTTON_PM_1 = 0x08;  //bit 0 of -/0/+ button
const int32_t BUTTON_PM_2 = 0x10;  //bit 1 of -/0/+ button
const int32_t BUTTON_OFF  = 0x20;  //offet button (- = 0, + = 1)

//OTHER
const int32_t NUM_CHAR_FUTABA = 40;
const int32_t NUM_BYTES_TUNER = 7;
