#pragma once

#include "wParameters.h"
#include "cMasterRS485.h"

class cEntry;
class cMasterRS485;
class cMyPanel;

class cMain : public wxFrame
{
	public: //constructor
		cMain(cEntry*, cMasterRS485*);
		void TerminateApp(void);

	private: //event handlers
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnKeyDown(wxKeyEvent& evt);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cMasterRS485* masterRS485;

	public: //other public variables
		wxCriticalSection main_guard; //main data guard

	private: //UI elements

		//IMAGES
		wxStaticBitmap* imgFutaba;

		//PANEL
		cMyPanel* panMain;

		//STATIC TEXTS
		wxStaticText* txtFutaba[40];

	public: //Shared Data

		uint8_t ButtonsTunerA;
		uint8_t ButtonsTunerB;
		uint8_t DataTunerA[7] = { 0 };
		uint8_t DataTunerB[7] = { 0 };

	private: //other data
		wxTimer* tRefreshTimer;

		wxDECLARE_EVENT_TABLE();
};

class cMyPanel: public wxPanel
{
	public:
		cMyPanel(cMain*, cMasterRS485*);
		void OnKeyDown(wxKeyEvent& event);
		void GetMasterData(void);

	private:
		cMasterRS485* masterRS485;

		uint8_t ButtonsTunerA;
		uint8_t ButtonsTunerB;
		uint8_t DataTunerA[7] = { 0 };
		uint8_t DataTunerB[7] = { 0 };

		DECLARE_EVENT_TABLE()
};

